all: draft-ietf-curdle-pkix.txt draft-josefsson-pkix-eddsa.txt draft-josefsson-pkix-eddsa.html draft-josefsson-pkix-newcurves.txt

# To prevent xml2rfc from opening any windows
unexport DISPLAY

%.txt: %.xml
	xml2rfc $< -o $@ --text

%.html: %.xml
	xml2rfc $< -o $@ --html
