<?xml version="1.0" encoding="US-ASCII"?>

<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY rfc2119 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc3279 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.3279.xml">
<!ENTITY rfc5280 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5280.xml">
<!ENTITY rfc5480 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5480.xml">
<!ENTITY rfc5639 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5639.xml">
<!ENTITY CURVES PUBLIC '' 'http://xml2rfc.ietf.org/public/rfc/bibxml3/reference.I-D.irtf-cfrg-curves.xml'>
<!ENTITY EDDSA PUBLIC '' 'http://xml2rfc.ietf.org/public/rfc/bibxml3/reference.I-D.irtf-cfrg-eddsa.xml'>
]>
<?rfc symrefs="yes"?>

<rfc category="info"
     ipr="trust200902"
     docName="draft-josefsson-pkix-newcurves-01" >
     
  <front>
    
    <title abbrev="PKIX OIDs for EdDSA">
      Using Curve25519 and Curve448 in PKIX
    </title>
    
    <author fullname="Simon Josefsson" initials="S." surname="Josefsson">
      <organization>SJD AB</organization>
      <address>
        <email>simon@josefsson.org</email>
      </address>
    </author>

    <date month="October" year="2015" />

    <keyword>Elliptic Curve Cryptography, Curve25519, Curve448,
    Goldilocks, X.509, PKIX, PKI, OID, ASN.1, Named Curve, EdDSA,
    Ed25519, Ed448</keyword>

    <abstract>

      <t>This document specify "named curve" object identifiers for
      the Curve25519 and Curve448 curves, for use in various X.509
      PKIX structures.</t>

    </abstract>

  </front>

  <middle>

    <section title="Introduction">

      <t>In <xref target="I-D.irtf-cfrg-curves"/>, the elliptic curves
      Curve25519 and Curve448 are described.  They are designed with
      performance and security in mind.  The curves may be used for
      Diffie-Hellman and Digital Signature operations.</t>
      
      <t>This RFC define ASN.1 "named curve" object identifiers for
      Curve25519 and Curve448, for use in the <xref
      target="RFC5280">Internet X.509 PKI</xref>.</t>

      <t>Rather than defining a new subject public key format for
      these two curves, this document re-use the existing ECDSA/ECDH
      public-key contained (described in section 2.3.5 of <xref
      target="RFC3279"/>) and introduce two new "named curve" OIDs.
      This approach is the same as for the <xref
      target="RFC5639">Brainpool curves</xref>.</t>

    </section>

    <section title="Requirements Terminology">

      <t>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
      NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and
      "OPTIONAL" in this document are to be interpreted as described
      in <xref target="RFC2119" />.</t>

    </section>
   
    <section title="Curve25519 and Curve448 Named Curve Identifier">

      <t>Certificates conforming to <xref target="RFC5280"/> may
      convey a public key for any public key algorithm.  The
      certificate indicates the algorithm through an algorithm
      identifier.  This algorithm identifier is an OID and optionally
      associated parameters.  Section 2.3.5 of <xref
      target="RFC3279"/> describe ECDSA/ECDH public keys, specifying
      the id-ecPublicKey OID.  This OID has the associated
      EcpkParameters parameters structure, which contains the
      namedCurve CHOICE.  Here we introduce two new OIDs for use in
      the namedCurve field.</t>

      <figure>
	<artwork><![CDATA[
	id-Curve25519   OBJECT IDENTIFIER ::= { 1.3.6.1.4.1.11591.15.1 }
	id-Curve448     OBJECT IDENTIFIER ::= { 1.3.6.1.4.1.11591.15.2 }
	id-Curve25519ph OBJECT IDENTIFIER ::= { 1.3.6.1.4.1.11591.15.3 }
	id-Curve448ph   OBJECT IDENTIFIER ::= { 1.3.6.1.4.1.11591.15.4 }
	]]></artwork>
      </figure>

      <t>The OID id-Curve25519 refers to Curve25519.  The OID
      id-Curve448 refers to Curve448.  Both curves are described in
      <xref target="I-D.irtf-cfrg-curves"/>.  The OIDs id-Curve25519ph
      and id-Curve448ph refers to Curve25519 and Curve448 when used
      with pre-hashing as Ed25519ph and Ed448ph described in <xref
      target="I-D.irtf-cfrg-eddsa"/>.</t>
      
      <t>The public key value encoded into the ECPoint value is the
      raw binary values described in <xref
      target="I-D.irtf-cfrg-curves"/>.</t>

    </section>
    
    <section anchor="ack"
             title="Acknowledgements">

      <t>Text and/or inspiration were drawn from <xref
      target="RFC5280"/>, <xref target="RFC3279"/>, <xref
      target="RFC5480"/>, and <xref target="RFC5639"/>.</t>

      <t>Several people suggested the utility of specifying OIDs for
      encoding Curve25519/Curve448 public keys into PKIX certificates,
      the editor of this document cannot take credit for this
      idea.</t>
      
    </section>

    <section title="IANA Considerations">

      <t>None.</t>

    </section>

    <section anchor="Security" title="Security Considerations">

      <t>The security considerations of <xref target='RFC3279' />,
      <xref target='RFC5280' />, <xref target='RFC5480' /> and <xref
      target="I-D.irtf-cfrg-curves"/> apply accordingly.</t>

    </section>

  </middle>

  <back>

    <references title="Normative References">

      &rfc2119;
      &rfc3279;
      &rfc5280;
      &rfc5480;
      &CURVES;
      &EDDSA;

    </references>

    <references title="Informative References">

      &rfc5639;

    </references>
   
  </back>
</rfc>
